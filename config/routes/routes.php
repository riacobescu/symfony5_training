<?php
// config/routes.php
use App\Controller\ListController;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

return function (RoutingConfigurator $routes) {
    $routes->add('company_list_php', '/list/php/1/{format}')
        ->controller([ListController::class, 'showCompaniesRoutePhp'])
        ->requirements(['format' => 'xml|json'])
    ;

    // ...
};

