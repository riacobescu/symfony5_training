<?php

namespace App\Controller;

use App\Services\CompanyService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends AbstractController
{


    const CONTENT_MAPPING = [
        'json' => 'application/json',
        'xml' => 'text/xml',
    ];
    /**
     * @Route("/list", name="list", methods="GET")
     */
    public function index()
    {
        $companies = [
            'Apple' => '$1.16 trillion USD',
            'Samsung' => '$298.68 billion USD',
            'Microsoft' => '$1.10 trillion USD',
            'Alphabet' => '$878.48 billion USD',
            'Intel Corporation' => '$245.82 billion USD',
            'IBM' => '$120.03 billion USD',
            'Facebook' => '$552.39 billion USD',
            'Hon Hai Precision' => '$38.72 billion USD',
            'Tencent' => '$3.02 trillion USD',
            'Oracle' => '$180.54 billion USD',
        ];

        return $this->render('list/index.html.twig', [
            'controller_name' => 'ListController',
            'companies' => $companies
        ]);
    }


    public function showCompanies(CompanyService $companyService)
    {
        $companies =  $companyService->getCompanyListing();
        return $this->render('list/custom.html.twig', [
            'controller_name' => 'ListController',
            'companies' => $companies
        ]);
    }

    /**
     * @Route("/list/response/{format}", name="list_response", methods="GET", requirements={"format"="json|xml"})
     */

    public function showCompaniesAsResponse($format, CompanyService $companyService)
    {
        $companies =  $companyService->getCompanyListingFormat($format);
        $contentType = self::CONTENT_MAPPING[$format];
        return new Response($companies, Response::HTTP_OK, ['Content-Type'=>$contentType]);
    }

    /**
     * @Route("/list/xml", name="list_xml", methods="GET")
     */

    public function showCompaniesAsXml(CompanyService $companyService)
    {
        $companies =  $companyService->getCompanyListingFormat('xml');
        return new Response($companies);
    }

    /**
     * @Route("/list/json", name="list_json", methods="GET")
     */

    public function showCompaniesAsJson(CompanyService $companyService)
    {
        $companies =  $companyService->getCompanyListing();
        return $this->json($companies);
    }

    public function showCompaniesRoutePhp($format, CompanyService $companyService)
    {
        $companies =  $companyService->getCompanyListingFormat($format);
        $contentType = self::CONTENT_MAPPING[$format];
        return new Response($companies, Response::HTTP_OK, ['Content-Type'=>$contentType]);
    }

    public function showCompaniesRouteXml($format, CompanyService $companyService)
    {
        $companies =  $companyService->getCompanyListingFormat($format);
        $contentType = self::CONTENT_MAPPING[$format];
        return new Response($companies, Response::HTTP_OK, ['Content-Type'=>$contentType]);
    }






}
