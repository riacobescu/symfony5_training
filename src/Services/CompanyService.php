<?php
/**
 * Created by PhpStorm.
 * User: razvan-iacobescu
 * Date: 21.07.2020
 * Time: 13:24
 */

namespace App\Services;


use App\Entity\Companies;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class CompanyService
{
    private $entityManager;

    public function __construct(EntityManagerInterface $em)
    {
        $this->entityManager = $em;
    }

    public function getCompanyListing(){
        $companies = $this->entityManager->getRepository(Companies::class)->findAll();
        $rsp = [];
        foreach ($companies as $item){
            $rsp[] = ['id'=>$item->getId() , 'name' => $item->getName()];
        }
        return $rsp;
    }

    public function getCompanyListingFormat($format = 'json'){
        $rsp = $this->getCompanyListing();
        $serializer = new Serializer([new ObjectNormalizer()], [new JsonEncoder(), new XmlEncoder()]);

        return $serializer->serialize($rsp,$format);
    }

}